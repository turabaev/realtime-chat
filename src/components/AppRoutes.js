import React, {useContext} from 'react';
import {Route, Routes, Navigate} from "react-router-dom";
import Chat from "./Chat";
import Login from "./Login";
import {Context} from "../index";
import {useAuthState} from "react-firebase-hooks/auth";

const AppRoutes = () => {
    const {auth} = useContext(Context)
    const [user] = useAuthState(auth)
    return (
        user ? (
            <Routes>
                <Route path={'/chat'} element={<Chat/>} />
                <Route
                    path="*"
                    element={<Navigate to="/chat" replace />}
                />
            </Routes>
        ) : (
            <Routes>
                <Route path={'/login'} element={<Login/>} />
                <Route
                    path="*"
                    element={<Navigate to="/login" replace />}
                />
            </Routes>
        )
    );
};

export default AppRoutes;