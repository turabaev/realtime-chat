import React, {useContext} from 'react';
import {Context} from "../index";
import {useAuthState} from "react-firebase-hooks/auth";

export const Navbar = () => {
    const {auth} = useContext(Context)
    const [user] = useAuthState(auth)

    return user ?
        <header>
            <button onClick={() => auth.signOut()} className={'header-btn'}>
                Выйти
            </button>
        </header>
        :
        <header>
            <button className={'header-btn'}>
                Логин
            </button>
        </header>
};