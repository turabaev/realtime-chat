import React, {useContext, useState} from 'react';
import {Context} from "../index";
import {useAuthState} from "react-firebase-hooks/auth";
import {useCollectionData} from "react-firebase-hooks/firestore";
import Loading from "./Loading";
import firebase from "firebase/compat/app";

const Chat = () => {
    const {auth, firestore} = useContext(Context)
    const [user] = useAuthState(auth)
    const [messages, loading] = useCollectionData(
        firestore.collection('messages').orderBy('createdAt')
    )

    const [value, setValue] = useState('')

    const sendMessage = async() => {
        await firestore.collection('messages').add({
            uid: user.uid,
            displayName: user.displayName,
            photoURL: user.photoURL,
            text: value,
            createdAt: firebase.firestore.FieldValue.serverTimestamp()
        })

        setValue('')
    }

    if (loading) {
        return <Loading/>
    }

    return (
        <div className={'container'}>
          <div className={'chat-wrapper'}>
              <div className={'messages'}>
                  {messages.map(message => {
                      return <div
                          className={'chat-item'}
                          key={message.uid}
                          style={{
                              margin: 10,
                              marginLeft: user.uid === message.uid ? 'auto': '10px',
                              width: 'fit-content'
                          }}
                      >
                          <div className={'avatar'}>
                              <img width={'100%'} height={'100%'} src={message.photoURL} alt=""/>
                          </div>
                          <div className={'user-info'}>
                              <span>{message.displayName}</span>
                              <p>{message.text}</p>
                          </div>
                      </div>
                  })}
              </div>
              <div className={'send-container'}>
                  <input type="text" placeholder={'Введите текст'} value={value} onChange={(e) => setValue(e.target.value)}/>
                  <button onClick={sendMessage}>Отправить</button>
              </div>
          </div>
        </div>
    );
};

export default Chat;