import React, {useContext} from 'react';
import {Context} from "../index";
import firebase from "firebase/compat/app";
import {GoogleAuthProvider} from "firebase/auth"

const Login = () => {
    const {auth} = useContext(Context)

    const login = async () => {
        const provider = new GoogleAuthProvider()
        const {user} = await auth.signInWithPopup(provider)

        console.log(user)
    }
    return (
        <div className={'container'}>
            <div className={'login'}>
                <button onClick={login}>
                    Войти с помощью Google
                </button>
            </div>
        </div>
    );
};

export default Login;