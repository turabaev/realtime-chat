import React, {createContext} from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import firebase from "firebase/compat/app";
import "firebase/compat/firestore";
import "firebase/compat/auth";

firebase.initializeApp({
    apiKey: "AIzaSyAGhX1V33RusCYttCCgOKmWjclaSR1YihI",
    authDomain: "realtime-chat-cf5dc.firebaseapp.com",
    projectId: "realtime-chat-cf5dc",
    storageBucket: "realtime-chat-cf5dc.appspot.com",
    messagingSenderId: "1065746355722",
    appId: "1:1065746355722:web:e16d841bc5a132c48c71f3",
    measurementId: "G-XG7P7QH0D8"
});

export const Context = createContext(null)

const auth = firebase.auth()
const firestore = firebase.firestore()

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <Context.Provider value={{
        firebase,
        auth,
        firestore
    }}>
        <App/>
    </Context.Provider>
);
