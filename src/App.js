import logo from './logo.svg';
import './App.css';
import {BrowserRouter} from "react-router-dom";
import React, {useContext} from "react";
import {Navbar} from "./components/Navbar";
import AppRoutes from "./components/AppRoutes";
import {Context} from "./index";
import {useAuthState} from "react-firebase-hooks/auth";
import Loading from "./components/Loading";

const App = () => {
    const {auth} = useContext(Context)
    const [user, loading, error] = useAuthState(auth)

    if (loading) {
        return <Loading/>
    }

  return (
      <BrowserRouter>
          <Navbar/>
          <AppRoutes/>
      </BrowserRouter>
  );
}

export default App;
